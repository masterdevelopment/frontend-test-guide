# Generate PDF
To generate PDF, the package [`md-to-pdf`](https://www.npmjs.com/package/md-to-pdf) has to be installed.

```
npm i -g md-to-pdf
```

Inside the project, the following command has to be run.
```
md-to-pdf ./README.md
```