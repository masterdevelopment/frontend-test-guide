# Frontend Test

## CSS
---
A continuación se muestra una imagen. El principal objetivo de este inciso es simple: se debe recrear usando solamente HTML5 y CSS3.

![alt text](./assets/css-image.png)

Los colores utilizados son los siguientes:

* Celeste: #6592CF
* Azul: #243D83
* Amarillo: #EEB850

## JS
---

Usando cualquier ambiente de ejecución de JS (Node.JS o Navegador) realiza el siguiente ejercicio.

Dado una cadena construida por las letras a, b y/o c, cambia las letras a con las letras b y viceversa. La posición de la letra c no debe ser alterada.

Ejemplo:
```
'acb' --> 'bca'
'aabacbaa' --> 'bbabcabb'
```

## App
---

Usando cualquier framework de frontend, se necesita que se realice una aplicación basada en el siguiente diseño:

https://xd.adobe.com/view/430a6ab4-b361-452c-4c28-d8531573a138-353b/

La aplicación está conformado por dos secciones principales.

1. El dashboard, donde se mostrara un listado de posts, conformados por título y cuerpo.
2. Una segunda sección donde se visualiza un post individual, con sus comentarios respectivos. 

La información a mostrar en cada pantalla, es servida de un API Rest. A continuación se detallan los endpoints:

### Listar posts
---
**GET** All posts
```
https://jsonplaceholder.typicode.com/posts
```
**Response**
``` JSON
[
  {
    "userId": 1,
    "id": 1,
    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
  },
  {
    "userId": 1,
    "id": 2,
    "title": "qui est esse",
    "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
  },
  ...
]
```

#### Listar post individual
---
**GET** Single post
```
https://jsonplaceholder.typicode.com/posts/:id
```

**Response**
``` JSON
{
  "userId": 1,
  "id": 1,
  "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
  "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
}
```

#### Obtener comentarios
---
**GET** Post comments
```
https://jsonplaceholder.typicode.com/posts/:id/comments
```

**Response**
``` JSON
[
  {
    "postId": 1,
    "id": 1,
    "name": "id labore ex et quam laborum",
    "email": "Eliseo@gardner.biz",
    "body": "laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium"
  },
  {
    "postId": 1,
    "id": 2,
    "name": "quo vero reiciendis velit similique earum",
    "email": "Jayne_Kuhic@sydney.com",
    "body": "est natus enim nihil est dolore omnis voluptatem numquam\net omnis occaecati quod ullam at\nvoluptatem error expedita pariatur\nnihil sint nostrum voluptatem reiciendis et"
  }
]
```

### Requerimientos
Es importante que esta aplicación cumpla los siguientes puntos:

* La aplicación debe cumplir con el diseño propuesto.
* La navegación dentro de la aplicación debe utilizar las siguientes rutas:
    * `/posts`: Aqui se mostrará el listado de posts, con su buscador.
    * `/singlepost/:id`: Aqui se mostrarán los posts individuales con sus respectivos comentarios. 
* El endpoint no soporta la búsqueda de posts por medio de un filtro, por lo que la lógica del buscador, debe implementarse del lado del cliente.
* En el diseño, hay animaciones en algunas secciones. Dependiendo del tiempo, se recomienda implementarlas.

### Puntos extras:

* Utilizar TDD para el desarrollo de la aplicación. Es importante considerar el tiempo de la prueba para incorporarlos al proyecto. 

## Entrega
---
Para la entrega del proyecto, se debe cree un repositorio público en github, con un README que contenga las instrucciones necesarias para levantarlo.

Enviar el link del repositorio al correo: work.with.us@masterdevel.com